.PHONY: all
all: *.adoc
	asciidoctor *.adoc

.PHONY: check
check: *.sh
	shellcheck --shell bash --enable all --exclude SC2250 $^

.PHONY: clean
clean:
	rm *.html
