#!/usr/bin/env nix-shell
#!nix-shell -i bash -p coreutils mosquitto

set -e

host="mqtt"

for arg in "$@"; do
  mosquitto_pub --host "$host" --null-message --retain --topic "$arg"
done
