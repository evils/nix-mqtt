#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p bc coreutils curl cacert git mosquitto
# shellcheck shell=bash

set -eu

# see the README in the script source for more info
script_source="https://gitlab.com/evils/nix-mqtt"
data_source="https://github.com/grahamc/nix-channel-monitor"

host="mqtt"

channels=("nixpkgs-unstable" "nixos-unstable" "nixos-unstable-small"
  "nixos-22.11" "nixos-22.11-small" "nixpkgs-22.11-darwin"
  "nixos-22.05" "nixos-22.05-small" "nixpkgs-22.05-darwin" "nixos-22.05-aarch64"
)

force=0
expire=
port=1883
args=("$@")
for ((i=0; i < $#; i++)); do
  case "${args[$i]}" in
    force|-f|--force|*fuck|f) force=1 ;;
    expire|-e|--expire|e|expiry|--expiry) if [[ -z "$expire" ]]; then expire="${args[((i+=1))]}"; fi ;;
    host|-h|--host|h|broker|-b|--broker|b) host="${args[((i+=1))]}" ;;
    port|-p|--port|p) port="${args[((i+=1))]}" ;;
    *) ;;
  esac
done

publish () {

  mosquitto_pub --retain --host $host --port $port --topic $1 --message $2

}

publish-expire () {

  if [[ "$expire" -gt 0 ]]; then
    mosquitto_pub --retain --host $host --port $port --topic $1 --message $2 \
      --protocol-version 5 --property publish message-expiry-interval "$expire"
  else
    publish $@
  fi
}

# convert unix timestamp to age of that timestamp in days
days () {
  now="$(date +%s)"

  if [[ -z "$1" ]]; then
    time="$now"
  else
    time="$1"
  fi

  diff="$(( now - time ))"

  days="$(echo "scale=3; " "${diff}" " / (60*60*24)" | bc)"

  printf "%3.2f\n" "${days}"
}

jitter () {
  sleep $(( 1 + (RANDOM % 4) ))
}

jitter
latest="$(git ls-remote --heads https://github.com/grahamc/nix-channel-monitor.git | grep "main" --max-count 1 | cut -f 1)"
local_latest="$(mosquitto_sub --host "$host" --topic "nix/channels/meta/commit" -C 1 -W 2 2>/dev/null || true)"
if (grep -q "$latest" <<< "$local_latest"); then
  outdated=0
else
  outdated=1
fi

echo "updating"

for channel in "${channels[@]}"; do

  topic="nix/channels/""$channel""/"

  printf "%s with:\n" "$channel"

  if [[ $outdated == 1 || $force == 1 ]]; then
    jitter
    v2="$(curl -s "https://raw.githubusercontent.com/grahamc/nix-channel-monitor/main/data/""$channel""/latest-v2")"
    if [[ "$v2" == "404: Not Found" ]]; then
      echo "skipping missing channel $channel"
      continue
    fi
    commit=$(cut -d " " -f 1 <<< "$v2")
    authored=$(cut -d " " -f 2 <<< "$v2")
    advanced=$(cut -d " " -f 3 <<< "$v2")

    local_commit="$(mosquitto_sub --host "$host" --topic "$topic""commit" -C 1 -W 2 2>/dev/null || true)"
    if [[ "$local_commit" != "$commit" || ("$local_commit" == "" && "$commit" != "") || $force == 1 ]]; then
      publish "$topic""advanced" "$advanced"
      publish "$topic""commit" "$commit"
      publish "$topic""commit-authored" "$authored"
      printf "\tadvanced:\t%s\n\tcommit:\t\t%s\n\tauthored:\t%s\n" "$advanced" "$commit" "$authored"
    fi
  fi

  local_advanced="$(mosquitto_sub --host "$host" --topic "$topic""advanced" -C 1 -W 2 2>/dev/null || true)"
  if [[ -z "$local_advanced" ]]; then
    echo "missing timestamp, skipping update of $channel age"
    continue
  fi
  age="$(days "$local_advanced")"
  publish-expire "$topic""age-in-days" "$age"
  printf "\tage:\t\t%s\n" "$age"

done

if [[ $outdated == 1 ]]; then
  publish "nix/channels/meta/commit" "$latest"
fi
publish "nix/channels/meta/checked" "$(date +%s)"

printf "updated on: %s\n\n" "$(date +%s)"

# add links to data and script source
# don't want to check this every time
# assuming this runs every 7 minutes,
# this should check about twice a day
if [[ $(( RANDOM % 100 )) == 0 || $force == 1 ]]; then
  local_data_source="$(mosquitto_sub --host "$host" --topic "nix/channels/meta/data-source" -C 1 -W 2 2>/dev/null || true)"
  if [[ "$local_data_source" != "$data_source" || $force == 1 ]]; then
    publish "nix/channels/meta/data-source" "$data_source"
    printf "set link to source\n\n"
  fi

  local_script_source="$(mosquitto_sub --host "$host" --topic "nix/channels/meta/script-source" -C 1 -W 2 2>/dev/null || true)"

  if [[ "$local_script_source" != "$script_source" || $force == 1 ]]; then
    publish "nix/channels/meta/script-source" "$script_source"
    printf "set link to source\n\n"
  fi
fi
